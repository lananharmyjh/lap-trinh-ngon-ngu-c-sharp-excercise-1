﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Forms;

namespace Grab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void payment_Click(object sender, EventArgs e)
        {
            const int KM5 = 20000;
            const int KM15 = 15000;
            const int KMNEXT = 10000;
            double Length = 0;
            double Discount = 0;

            double.TryParse(TxbLength.Text, out Length);
            double.TryParse(TxbDiscount.Text, out Discount);


            double remainLength = Length;
            double fee = 0;
            if (remainLength > 0 && remainLength >= 5)
            {
                fee += 5 * KM5;
                remainLength = remainLength - 5;
            }
            else
            {
                fee += remainLength * KM5;
                remainLength = 0;
            }

            if (remainLength > 0 && remainLength >= 15)
            {
                fee += 15 * KM15;
                remainLength = remainLength - 15;
            }
            else
            {
                fee += remainLength * KM15;
                remainLength = 0;
            }


            fee += remainLength * KMNEXT;

            fee -= fee * (Discount / 100);

            //TxbFee.Text = fee.ToString("### ### ### ### ### ### ### ### ###");
            //fee = 13.42342;
            TxbFee.Text = string.Format("{0:#,0.##########}", fee);

            //var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            //nfi.NumberGroupSeparator = " ";
            //string formatted = fee.ToString("#,0.#", nfi); // "1 234 897.11";
            //TxbFee.Text = formatted;

        }

        private void TxbLength_TextChanged(object sender, EventArgs e)
        {
            try
            {
            if (Convert.ToInt32(TxbLength.Text) > 100)
                { TxbDiscount.Text = "10"; }
                else
                { TxbDiscount.Text = "0"; }
            }
            catch { TxbDiscount.Text = "0"; };
        }
    }
}
